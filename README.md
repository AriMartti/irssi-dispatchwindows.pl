# irssi dispatchwindows.pl

Irssi script which makes possible to handle tens of Irssi windows by using 
simple "<cmdchar>number" -notation. Script is original written by Sebastian 
'yath' Schmidt and Tom Wesley in 2002. Script is published under GPLv2.